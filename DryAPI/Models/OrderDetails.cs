﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DryAPI.Models
{
    public class OrderDetails
    {
        public List<OrderVM> Order { get; set; }
       public string UserId { get; set; }
        public int DryId { get; set; }
        public string Token { get; set; }
    }
}