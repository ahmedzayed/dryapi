﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DryAPI.Models
{
    public class OrderVM
    {
        public int ServicesId { get; set; }
        public int Quantity { get; set; }
    }
}