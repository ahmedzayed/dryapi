﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DryAPI.Models
{
    public class FireBaseData
    {
        public string ShortDesc { get; set; }
        public string IncidentNo { get; set; }
        public string Description { get; set; }
    }
}