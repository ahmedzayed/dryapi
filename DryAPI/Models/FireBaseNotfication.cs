﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DryAPI.Models
{
    public class FireBaseNotfication
    {
        public string title { get; set; }
        public string text { get; set; }
    }
}