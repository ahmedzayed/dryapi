﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using DryAPI.Models;
using DryAPI.Providers;
using DryAPI.Results;
using System.Net;
using System.Linq;
using DryAPI.Core;
using DryAPI.VM.VM;
using DryAPI.Helper;

namespace DryAPI.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }



        [HttpGet]
        [Route("Login")]
        public async Task<HttpResponseMessage> Login(string UserName, string Password,string Token)
        {
            //try
            //{
                

                var user = await UserManager.FindAsync(UserName, Password);


            var UserInfo = "SP_Updatetoken".ExecuParamsSqlOrStored(false,"Token".KVP(Token), "Id".KVP(user.Id)).AsNonQuery();

            UserInfoVM U = new UserInfoVM()
                {
                    Token=Token,
                    UserName = user.UserName,
                    Id = user.Id,
                    UserType=user.UserType,
                                        AreaName = user.AreaName,
                    Email = user.Email,
                   
                    PhoneNumber = user.PhoneNumber,

                    Lat = user.Lat,
                    Lang = user.Lang,
                    Name = user.Name
                };
                if (U != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, U);
                }
                else
                {
                    Result R = new Result();
                    R.Massage = "من فضلك ادخل البيانات بالشكل الصحيح";
                    R.Status = false;
                    R.Description = "من فضلك تواصل مع المسؤل";
                    return Request.CreateResponse(R);
                }

            //}
            //catch
            //{
            //    Result R = new Result();
            //    R.Massage = "من فضلك ادخل البيانات بالشكل الصحيح";
            //    R.Status = false;
            //    R.Description = "من فضلك تواصل مع المسؤل";
            //    return Request.CreateResponse(R);
            //}
        }



        
      
    
        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        [HttpPost]
        public async Task<HttpResponseMessage> Register(string Name,string Phone,string UserName,string Email,string Password,string Lat,string Lang,string AreaName,int UserType,string Token)
        {
           
            var user = new ApplicationUser() { UserName = UserName, Email = Email,Name=Name,Lang=Lang,Lat=Lat,PhoneNumber=Phone,AreaName=AreaName,UserType=UserType ,Token=Token};

            IdentityResult result = await UserManager.CreateAsync(user, Password);

            if (!result.Succeeded)
            {
                Result R = new Result();
                R.Massage = result.Errors.FirstOrDefault().ToString();
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

      
        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; set; }
            public string ProviderKey { get; set; }
            public string UserName { get; set; }

            public IList<Claim> GetClaims()
            {
                IList<Claim> claims = new List<Claim>();
                claims.Add(new Claim(ClaimTypes.NameIdentifier, ProviderKey, null, LoginProvider));

                if (UserName != null)
                {
                    claims.Add(new Claim(ClaimTypes.Name, UserName, null, LoginProvider));
                }

                return claims;
            }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                if (identity == null)
                {
                    return null;
                }

                Claim providerKeyClaim = identity.FindFirst(ClaimTypes.NameIdentifier);

                if (providerKeyClaim == null || String.IsNullOrEmpty(providerKeyClaim.Issuer)
                    || String.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                    ProviderKey = providerKeyClaim.Value,
                    UserName = identity.FindFirstValue(ClaimTypes.Name)
                };
            }
        }

        private static class RandomOAuthStateGenerator
        {
            private static RandomNumberGenerator _random = new RNGCryptoServiceProvider();

            public static string Generate(int strengthInBits)
            {
                const int bitsPerByte = 8;

                if (strengthInBits % bitsPerByte != 0)
                {
                    throw new ArgumentException("strengthInBits must be evenly divisible by 8.", "strengthInBits");
                }

                int strengthInBytes = strengthInBits / bitsPerByte;

                byte[] data = new byte[strengthInBytes];
                _random.GetBytes(data);
                return HttpServerUtility.UrlTokenEncode(data);
            }
        }

        #endregion
    }
}
