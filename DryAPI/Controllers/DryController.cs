﻿using DryAPI.Core;
using DryAPI.Helper;
using DryAPI.Models;
using DryAPI.VM.VM;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace DryAPI.Controllers
{
    [System.Web.Http.RoutePrefix("api/Dry")]

    public class DryController : ApiController
    {
        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllDry")]
        public HttpResponseMessage GetAllDry()
        {
            var DryList = "Sp_GetAllDry".ExecuParamsSqlOrStored(false).AsList<DryInfoVM>();
            if (DryList != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DryList);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllDryServicesType")]
        public HttpResponseMessage GetAllDryServicesType()
        {
            var DryList = "Sp_GetAllDryServicesType".ExecuParamsSqlOrStored(false).AsList<ServicesTypeVM>();
            if (DryList != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DryList);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }

        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllDryServices")]
        public HttpResponseMessage GetAllDryServices(int DryId=0,int ServiceTypeId=0)
        {
            var DryList = "Sp_GetAllDryServices".ExecuParamsSqlOrStored(false,"DryId".KVP(DryId), "ServiceTypeId".KVP(ServiceTypeId)).AsList<ServicesInfoVM>();
            if (DryList != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DryList);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [System.Web.Http.HttpPost, System.Web.Http.Route("AddDry")]
        public HttpResponseMessage AddDry(string Name,string PhoneNumber,string ManageId, string Address,string Lat ,string Lang,string WorkingTime,string Details, HttpPostedFile File)
        {
            string Image = "";
            if (File.ContentLength > 0)
            {
                var file = File;

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = System.Web.HttpContext.Current.Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                  Image  = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }

            var DryList = "SP_AddDB_A40639_DryClean".ExecuParamsSqlOrStored(false, "Name".KVP(Name), "Details".KVP(Details),
                "LangId".KVP(Lang),"LatId".KVP(Lat),"ManageId".KVP(ManageId),"PhoneNumber".KVP(PhoneNumber),"WorkingTime".KVP(WorkingTime),"Image".KVP(Image),"Case".KVP(true)).AsNonQuery();
            if (DryList != 0)
            {
                Result R = new Result();
                R.Massage = "تم الاضافة بنجاح";
                R.Status = true;
                R.Description = "تم الحفظ فى قاعده البيانات";
                return Request.CreateResponse(R);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [HttpPost, Route("AddPriceServices")]
        public HttpResponseMessage AddPriceServices(int ServicesId, int ServicesTypeId, decimal Price, int DryId)
        {
            var DryList = "SP_AddPriceServices".ExecuParamsSqlOrStored(false, "ServicesId".KVP(ServicesId), "ServicesTypeId".KVP(ServicesTypeId),
                "Price".KVP(Price), "DryId".KVP(DryId)).AsNonQuery();
            if (DryList != 0)
            {
                Result R = new Result();
                R.Massage = "تم الاضافة بنجاح";
                R.Status = true;
                R.Description = "تم الحفظ فى قاعده البيانات";
                return Request.CreateResponse(R);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [HttpPost, Route("AddOrder")]
        public HttpResponseMessage AddOrder(OrderDetails Order )
        {
            try
            {

                var AddOrder = "SP_AddOrder".ExecuParamsSqlOrStored(false, "UserId".KVP(Order.UserId), "OrderCase".KVP(1),
             "OrderDate".KVP(DateTime.Now),"Total".KVP(0),"Id".KVP(0)).AsList<OrderVMH>();

                //SendNotificationFromFirebaseCloud(Order.Token);


                foreach (var item in Order.Order)
                {
                 
                    var GetPrice= "SP_GetPriceServices".ExecuParamsSqlOrStored(false, "DryId".KVP(Order.DryId), "ServicesId".KVP(item.ServicesId)).AsList<PriceServiceVM>();

                    decimal Total = item.Quantity * GetPrice.FirstOrDefault().Price;

                    var AddOrderDetails = "SP_AddOrderDetails".ExecuParamsSqlOrStored(false, "ServicesId".KVP(item.ServicesId), "Quantity".KVP(item.Quantity),
                "OrderId".KVP(AddOrder.FirstOrDefault().Id),"Total".KVP(Total),"Id".KVP(0)).AsNonQuery();
                    
                }
                var OrderTotal = "SP_GetTotal".ExecuParamsSqlOrStored(false, "OrderId".KVP(AddOrder.FirstOrDefault().Id)).AsList<OrderDetailsVM>();
            decimal TotalOrder = OrderTotal.Sum(a => a.Total);


                "SP_AddOrder".ExecuParamsSqlOrStored(false, "UserId".KVP(Order.UserId), "OrderCase".KVP(1),
           "OrderDate".KVP(DateTime.Now), "Total".KVP(TotalOrder), "Id".KVP(AddOrder.FirstOrDefault().Id)).AsNonQuery();

                
               
                    Result R = new Result();
                    R.Massage = "تم الحفظ بنجاح";
                    R.Status = true;
                    R.Description = "تم حفظ الطلب";
                    return Request.CreateResponse(R);



        }
            catch
            {
                Result R = new Result();
        R.Massage = "لم يتم الحفظ من فضلك ادخل البانات بالشكل الصحيح";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

}


        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllOrder")]
        public HttpResponseMessage GetAllOrder(string UserId)
        {
            var DryList = "Sp_GetAllOrder".ExecuParamsSqlOrStored(false,"Id".KVP(UserId)).AsList<OrdersVM>();
            if (DryList != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DryList);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        [System.Web.Http.HttpGet, System.Web.Http.Route("GetAllOrderDetails")]
        public HttpResponseMessage GetAllOrderDetails(int OrderId=0)
        {
            var DryList = "Sp_GetAllOrderDetails".ExecuParamsSqlOrStored(false,"OrderId".KVP(OrderId)).AsList<OrdersDetailsVM>();
            if (DryList != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, DryList);
            }
            else
            {
                Result R = new Result();
                R.Massage = "لا يوجد بيانات";
                R.Status = false;
                R.Description = "من فضلك تواصل مع المسؤل";
                return Request.CreateResponse(R);

            }

        }


        public void SendNotificationFromFirebaseCloud(string Token)
        {

            var result = "-1";
            var webAddr = "https://fcm.googleapis.com/fcm/send";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);

            httpWebRequest.ContentType = "application/json";
            var firebasekey = System.Configuration.ConfigurationManager.AppSettings["FireBaseKey"];
            httpWebRequest.Headers.Add(HttpRequestHeader.Authorization, "key=" + firebasekey);
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                FireBaseData data = new FireBaseData();
                data.ShortDesc = "Notfication";
                data.IncidentNo = "1";
                data.Description = "تم انشاء الطلب وموظف المغسلة فى الطريق اليك";
                FireBaseNotfication notfication = new FireBaseNotfication();
                notfication.title = "Notfication";
                notfication.text = "تم انشاء الطلب وموظف المغسلة فى الطريق اليك";

                FireBaseObject obj = new FireBaseObject();
                obj.to = Token;
                obj.data = data;
                obj.notification = notfication;

                obj.sound = "default";
                string strNJson = JsonConvert.SerializeObject(obj);

                streamWriter.Write(strNJson);
                streamWriter.Flush();
            }

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    result = streamReader.ReadToEnd();
            //    Console.WriteLine(streamReader.ReadToEnd());
            //}

           
        }










    }
}
