﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DryAPI.VM.VM
{
   public class DryServicesVM
    {
        public int Id { get; set; }
        public string ServicesName { get; set; }
        public decimal Price { get; set; }
        public string ServicesImage { get; set; }
    }
}
