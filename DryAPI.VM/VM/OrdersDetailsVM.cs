﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DryAPI.VM.VM
{
   public class OrdersDetailsVM
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public string ServiceName { get; set; }
        public int Quantity { get; set; }
        public decimal Total { get; set; }
    }
}
