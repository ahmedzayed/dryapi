﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DryAPI.VM.VM
{
   public class OrderVMH
    {
        public int Id { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderCase { get; set; }
        public int DryId { get; set; }
    }
}
