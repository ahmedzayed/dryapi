﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DryAPI.VM.VM
{
   public class UserInfoVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Lat { get; set; }
        public string Lang { get; set; }
        public string AreaName { get; set; }
        public string PhoneNumber { get; set; }
        public int UserType { get; set; }
        public string Token { get; set; }
    }
}
